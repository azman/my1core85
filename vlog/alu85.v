module alu85 (iS, iA, iB, iF, oY, oF);

parameter BITS = 8;
parameter FLAG_S = 7;
parameter FLAG_Z = 6;
parameter FLAG_A = 4;
parameter FLAG_P = 2;
parameter FLAG_C = 0;

input[2:0] iS;
input[BITS-1:0] iA, iB, iF;
output[BITS-1:0] oY, oF;

wire[BITS-1:0] tA,tL,tADD,tSUB;
wire tF;
wire[BITS-1:0] tC,tB;
wire tACL,tCYL,tACA,tCYA;

// dummy signals
wire[BITS-1:0] sP,dP;

// option to use carry in!
assign tF = iS[0] & iF[FLAG_C];

// select arithmetic op
assign tA = iS[1] ? tSUB : tADD;
// select alu op
assign oY = iS[2] ? tL : tA;

// these always updates accordingly
assign oF[FLAG_S] = oY[FLAG_S]; // sign flag
assign oF[FLAG_Z] = ~|oY; // zero flag
assign oF[FLAG_P] = ~^oY; // even-parity flag (1 if even 1's)
// these depends on operations
assign oF[FLAG_A] = iS[2] ? tACL : tACA; // auxiliary carry flag
assign oF[FLAG_C] = iS[2] ? tCYL : tCYA; // carry flag
// AND sets, XOR/OR clears, CMP takes SUB results
assign tACL = (~(iS[1]|iS[0]))|(iS[1]&iS[0]&tB[3]);
// AND/XOR/OR always clears, CMP takes SUB results
assign tCYL = (iS[1]&iS[0]&tB[7]);
// arithmetic flag assignments
assign tACA = iS[1] ? tB[3] : tC[3];
assign tCYA = iS[1] ? tB[7] : tC[7];

// adder circuit
iadd doadd (iA,iB,tF,tADD,tC,sP);
// subtract circuit
isub dosub (iA,iB,tF,tSUB,tB,dP);
// logic circuit
bitw logic (iS[1:0],iA,iB,tL);

endmodule
