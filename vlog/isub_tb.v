module isub_tb ();

parameter BITS = 8;

reg[BITS-1:0] iJ, iK, iB;
wire[BITS-1:0] oD, oB, oP;

integer numC, ecnt;
reg[BITS-1:0] repA, repB;
reg[BITS-1:0] test, ends;
reg[BITS-1:0] tD;
reg tB;
reg signed[BITS:0] chkD;

initial begin
	$write("-- Start test for %g-bit integer subtractor... ",BITS);
	ecnt = 0;
	ends = 2**BITS;
	for (numC=0;numC<2;numC=numC+1) begin
		iB[0] = numC;
		for (repA=0;repA<ends;repA=repA+1) begin
			iJ = repA;
			for (repB=0;repB<ends;repB=repB+1) begin
				iK = repB;
				#5;
				test = repA - repB - numC;
				{ tB,tD } = test;
				chkD = {oB[BITS-1],oD};
				if ((tB!==oB[BITS-1])||(tD!==oD)) begin
					ecnt = ecnt + 1;
					$write("\n** [ERROR] {%g-%g-%b} ",iJ,iK,iB[0]);
					$write("= {%g:%b,%g} ",test,tB,tD);
					$write(" got {%g:%b,%g} (%d)\n",chkD,oB[BITS-1],oD,ecnt);
				end
				#5;
			end
		end
		$write(".");
	end
	$display(" done.");
	if (ecnt==0) begin
		$display("-- Module isub (%g-bits) verified.",BITS);
	end
	else begin
		$display("** Module isub with error(s) {ErrorCount=%g}",ecnt);
	end
	$finish;
end

isub #(BITS) dut (iJ,iK,iB,oD,oB,oP);

endmodule
