module bitw_tb ();

parameter TB_BITS = 8;

reg[1:0] dS; // 00=AND, 01=XOR, 10=OR, 11=PASS
reg[TB_BITS-1:0] dA, dB, cY;
wire[TB_BITS-1:0] mY;

integer loop, ecnt;

initial begin
	$display("[%8d] Begin test for %g-bit logic...",$time,TB_BITS);
	ecnt = 0;
	for (loop=0;loop<2**18;loop=loop+1) begin
		{dS,dA,dB} = loop;
		#5;
		case (dS)
			2'b00: begin // AND
				cY = dA & dB;
				if (mY!==cY) begin
					ecnt = ecnt + 1;
					$display("** {%b&%b}={%b}got{%b} (%d)",dA,dB,cY,mY,ecnt);
				end
			end
			2'b01: begin // XOR
				cY = dA ^ dB;
				if (mY!==cY) begin
					ecnt = ecnt + 1;
					$display("** {%b^%b}={%b}got{%b} (%d)",dA,dB,cY,mY,ecnt);
				end
			end
			2'b10: begin // OR
				cY = dA | dB;
				if (mY!==cY) begin
					ecnt = ecnt + 1;
					$display("** {%b|%b}={%b}got{%b} (%d)",dA,dB,cY,mY,ecnt);
				end
			end
			2'b11: begin // PASS
				cY = dA;
				if (mY!==cY) begin
					ecnt = ecnt + 1;
					$display("** {%b}={%b}got{%b} (%d)",dA,cY,mY,ecnt);
				end
			end
		endcase
		#5;
	end
	$display("[%8d] End test {ErrorCount=%g}",$time,ecnt);
	if (ecnt==0) begin
		$display("-- Module bit_logic verified!");
	end
	$finish;
end

bitw dut (dS,dA,dB,mY);

endmodule
